import { GraphQLClient } from 'graphql-request'

const endpoint = 'https://app-dev.fullhouse.io/graphql'

const graphQLClient = new GraphQLClient(endpoint)

export default graphQLClient
