import graphQLClient from './graphql-client'
import { print } from 'graphql/language/printer'

const apiRequest = async (query, variables, headers = null) => {
  return await graphQLClient.request(print(query), variables)
}

export default apiRequest
