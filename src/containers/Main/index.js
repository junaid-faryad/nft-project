import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import Layout from '../Layout'
import theme from 'components/Theme'

const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  /* height: calc(100vh - 48px); height is being set from layout parent component */
  overflow: hidden auto;
  flex-direction: ${props => (props.hideHeader ? 'row' : 'column')};
`

const MainWrapper = styled.div`
  display: flex;
  max-width: ${theme.constants.maxWidth + 'px'};
  flex-direction: ${props => (props.hideHeader ? 'row' : 'column')};
  margin: 0px auto;
  position: relative;
  width: 100%;
  padding: 0px;
  height: 100%;
`

const MainContainer = ({
  v2 = false,
  title,
  headerActions,
  children,
  isAdminTemplate = false,
  fixedHeaderResponsive = false,
  hideHeader = false,
  fullWidth = false,
  hideBottomBorder = false,
}) => {
  return (
    <Layout>
      <Container hideHeader={hideHeader} id="main-container">
        <MainWrapper fullWidth={fullWidth} hideHeader={hideHeader}>
          {children}
        </MainWrapper>
      </Container>
    </Layout>
  )
}

MainContainer.propTypes = {
  title: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  headerActions: PropTypes.node,
  children: PropTypes.node,
  v2: PropTypes.bool,
  isAdminTemplate: PropTypes.bool,
  fixedHeaderResponsive: PropTypes.bool,
  fullWidth: PropTypes.bool,
  hideBottomBorder: PropTypes.bool,
}

export default MainContainer
