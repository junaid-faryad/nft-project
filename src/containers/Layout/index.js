import React from 'react'
import PropTypes from 'prop-types'
import Image from 'next/image'
import { useRouter } from 'next/router'

// Local imports
import { Grow, MainContainer, SAppBar, SToolbar, Wrapper } from './components'
// import SocialIcons from 'components/Elements/SocialIcons'

export const Layout = ({ children }) => {
  const router = useRouter()
  return (
    <MainContainer>
      <SAppBar color="inherit" position="fixed" elevation={0}>
        <SToolbar disableGutters>
          <Grow>
            <Image
              onClick={() => router.push('/')}
              alt="full-house-logo"
              src="/logo.png"
              width="140px"
              height="53px"
            />
            {/* <SocialIcons marginRight /> */}
          </Grow>
        </SToolbar>
      </SAppBar>
      <Wrapper>{children}</Wrapper>
    </MainContainer>
  )
}

Layout.propTypes = {
  children: PropTypes.shape({}).isRequired,
}

export default Layout
