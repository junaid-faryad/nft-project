import React from 'react'
import styled from 'styled-components'
import { AppBar, Toolbar, Drawer, IconButton } from '@material-ui/core'
import theme from 'components/Theme'
import { FontWeights } from 'components/Typography'
import { constants } from 'components/Theme/constants'

const drawerWidth = '100%'

export const MainContainer = styled(({ isImpersonated, ...rest }) => <div {...rest} />)`
  display: flex;
  flex: 1;
  min-height: calc(100vh);
  width: 100%;
  justify-content: center;
  overflow-x: auto;
  padding-top: ${theme.constants.appBarHeight}px;
  background-color: ${theme.colors.n80};
  color: ${theme.colors.white};

  ${props => props.theme.breakpoints.down('md')} {
    min-width: auto;
    width: 100%;
  }
  ${props => props.theme.breakpoints.down('sm')} {
    min-width: 0px;
    padding-top: ${theme.constants.mobileAppBarHeight}px;
  }
`

export const SAppBar = styled(({ isImpersonated, ...rest }) => <AppBar {...rest} />)`
  && {
    height: ${theme.constants.appBarHeight}px;
    padding: 0;
    z-index: ${props => props.theme.zIndex.drawer - 2};
    box-shadow: none;
    width: 100%;
    ${props => props.theme.breakpoints.down(constants.tabletWidth)} {
      width: 100%;
    }
    ${props => props.theme.breakpoints.down('sm')} {
      height: ${theme.constants.mobileAppBarHeight}px;
    }
  }
`
export const SToolbar = styled(Toolbar)`
  && {
    height: ${theme.constants.appBarHeight}px;
    min-height: ${theme.constants.appBarHeight}px;
    background-color: ${theme.colors.black};
    box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.51);
    /* border-bottom: 1px solid ${theme.colors.n20}; */
    ${props => props.theme.breakpoints.down(constants.tabletWidth)} {
      height: ${theme.constants.mobileAppBarHeight}px;
    }
  }
`

export const SDrawer = styled(Drawer)`
  && {
    .MuiDrawer-paper {
      width: ${drawerWidth};
      max-width: 400px;
    }

    .close {
      position: absolute;
      z-index: 1;
      top: 0px;
      right: 16px;
    }
  }
`

export const MenuButton = styled(IconButton)`
  && {
    margin-right: 8px;
    min-width: 50px;
    color: ${theme.colors.white};

    ${props => props.theme.breakpoints.up(constants.tabletWidth)} {
      display: none;
    }
  }
`

export const DesktopBar = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  ${props => props.theme.breakpoints.down('sm')} {
    display: none;
  }
`

export const MobileBar = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  height: 100vh;
`

export const DrawerLeft = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 80%;
  padding-top: 4px;
`

export const DrawerRight = styled.div`
  display: flex;
  width: 20%;
`

export const MobileHelp = styled.div`
  display: flex;
  width: 100%;
  background-color: ${theme.colors.n20};
  padding: 28px 24px;
  cursor: pointer;
`

export const MenuItems = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 12px;
  .divider {
    margin: 4rem 2rem 2rem;
    box-sizing: border-box;
    display: block;
  }
`

export const LogoContainer = styled.div`
  width: 100%;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  display: flex;
  min-height: 100px;

  ${props => props.theme.breakpoints.down(constants.tabletWidth)} {
    display: flex;
    margin: 0;
  }
`
export const MobileLogoContainer = styled.div`
  height: 48px;
  border-bottom: 1px solid ${theme.colors.n20};
  display: flex;
  align-items: center;
  padding-left: 24px;
`

export const Link = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-size: 14px;
  box-sizing: border-box;
  margin: 0px;
  line-height: 20px;
  font-weight: ${FontWeights.semiBold};
  padding: 14px 20px 14px 28px;
  cursor: pointer;
  position: relative;

  :hover {
    background-color: ${theme.colors.n10};
  }

  a {
    display: flex;
    align-items: center;
    color: ${({ active }) => (active ? theme.colors.primaryColor : theme.colors.n60)};
    text-decoration: none;
    svg {
      margin-right: 12px;
      opacity: ${({ active }) => (active ? '1' : '0.8')};
    }
  }

  ${props => props.theme.breakpoints.down('sm')} {
    color: ${({ active }) => (active ? theme.colors.primaryColor : theme.colors.n60)};
  }
  .left-bar {
    width: 9px;
    background-color: ${theme.colors.primaryColor};
    border-radius: 12px;
    position: absolute;
    top: 0px;
    left: 0px;
    height: 100%;
    display: flex;
  }
  .nav-icon {
    color: ${theme.colors.black};
    opacity: 0.6;
    margin-right: 16px;
    font-size: 0.8rem;
  }

  ${props => props.theme.breakpoints.down('sm')} {
    margin: 0px 16px;
  }
`
export const Grow = styled.div`
  flex-grow: 1;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 5%;
  .connect-button {
    padding-right: 20px;
  }
  .avatar {
    padding: 0px 20px;
  }
  img {
    cursor: pointer;
  }
`

export const NameContainer = styled.div`
  padding-left: 10px;
  padding-right: 20px;
  cursor: pointer;
  color: ${theme.colors.white};
  font-weight: 500;
  line-height: 16px;
  padding-top: 2px;
  ${props => props.theme.breakpoints.down('xs')} {
    display: none;
  }
`

export const MenuItem = styled.div`
  height: 44px;
  padding-left: 20px;
  display: flex;
  align-items: center;
  cursor: pointer;

  &:hover {
    background-color: ${theme.colors.n10};
  }

  .MuiTypography-caption {
    display: flex;
    align-items: center;
  }

  .MuiSvgIcon-root {
    margin-right: 8px;
    font-size: 20px;
    opacity: 0.6;
    color: ${theme.colors.black};
    line-height: 20px;
  }
`

export const Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 100%;
  overflow-x: hidden;
  position: relative;
  flex-direction: column;
  transition: ${({ open, leftNavOpen }) =>
    open || leftNavOpen
      ? props =>
          props.theme.transitions.create(['width', 'margin'], {
            easing: props.theme.transitions.easing.sharp,
            duration: props.theme.transitions.duration.leavingScreen,
          })
      : props =>
          props.theme.transitions.create(['width', 'margin'], {
            easing: props.theme.transitions.easing.easeOut,
            duration: props.theme.transitions.duration.enteringScreen,
          })};
`

// Left Nav menu css
export const NavDrawer = styled(Drawer)`
  && {
    width: ${({ open }) => (open ? theme.constants.leftNavDrawerWidth : 24)}px;
    flex-shrink: 0;
    z-index: ${props => props.theme.zIndex.drawer + 2};
    position: relative;
    transition: ${props =>
      props.theme.transitions.create('width', {
        easing: props.theme.transitions.easing.sharp,
        duration: props.theme.transitions.duration.enteringScreen,
      })};
  }
  ul {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  .menu-icon {
    position: absolute;
    padding: 3px;
    border: 1px solid ${theme.colors.n20};
    right: 10px;
    top: 40px;
    background-color: ${theme.colors.white};
    box-shadow: 0px -4px 8px rgba(0, 0, 0, 0.05);
    :hover {
      background-color: ${theme.colors.primaryColor};
      color: ${theme.colors.white};
      border-color: ${theme.colors.primaryColor};
    }
    svg {
      transition: rotate 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
        transform 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
      transform: ${({ open }) => (!open ? 'rotate(180deg)' : 'rotate(0deg)')};
    }
  }
  .open {
    width: ${theme.constants.leftNavDrawerWidth}px;
    transition: ${props =>
      props.theme.transitions.create('width', {
        easing: props.theme.transitions.easing.sharp,
        duration: props.theme.transitions.duration.enteringScreen,
      })};
  }
  .close {
    transition: ${props =>
      props.theme.transitions.create('width', {
        easing: props.theme.transitions.easing.sharp,
        duration: props.theme.transitions.duration.leavingScreen,
      })};
    width: 40px;
  }
  .toolbar {
    flex-shrink: 0;
  }
  .MuiDrawer-paper {
    background-color: ${theme.colors.white};
  }
  ${props => props.theme.breakpoints.down(constants.tabletWidth)} {
    display: none;
  }
  .MuiList-padding {
    padding-top: 8px;
  }
  .hidden {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s linear 0.2s, opacity 0.2s linear;
  }
  .show {
    visibility: visible;
    opacity: 1;
    transition-delay: 0.4s;
    transition: visibility 0s linear 0.2s, opacity 0.2s linear;
  }
`

export const NavFooter = styled.div`
  border-top: 1px solid ${theme.colors.n20};
  margin-top: auto;
  display: flex;
  align-items: center;
  justify-content: center;
`

// export const LogoStyledDark = styled(Logo)`
//   fill: ${theme.colors.black};
//   height: 24px;
//   cursor: pointer;
// `

// export const LogoStyledLight = styled(Logo)`
//   height: 24px;
//   fill: ${theme.colors.white};
//   cursor: pointer;
// `
