const appBarHeight = 84
const mobileAppBarHeight = 84
const drawerWidth = 260
const leftNavDrawerWidth = 250
const tabletWidth = 1024
const maxWidth = 1200

export const constants = {
  appBarHeight,
  mobileAppBarHeight,
  drawerWidth,
  tabletWidth,
  leftNavDrawerWidth,
  maxWidth,
}
