import React from 'react'
import get from 'lodash/get'
import isString from 'lodash/isString'
import toLower from 'lodash/toLower'
import includes from 'lodash/includes'
import isEmpty from 'lodash/isEmpty'
import styled from 'styled-components'

import CardDetails from 'components/Elements/CardDetails'
import CarouselComponent from 'components/Media/MediaCarousel/SliderMedia'
import theme from 'components/Theme'
import { H3 } from 'components/Typography'

const Container = styled.div`
  display: flex;
  flex-grow: 1;
  margin: 32px auto 20px;
  flex-direction: column;
  ${props => props.theme.breakpoints.down('xs')} {
    margin-top: 12px;
  }
`

const Heading = styled.div`
  display: flex;
  width: 100%;
  ${props => props.theme.breakpoints.down('xs')} {
    padding: 8px 12px;
  }
`

const SliderMediaStyle = styled.div`
  max-width: 1270px;
  margin-bottom: 8px;
  .carousel {
    margin-top: 12px;
    text-align: center;
    .thumbs-wrapper {
      margin: 0px;
      .thumbs {
        width: 100%;
        padding: 0px;
        margin: 6px 0px;
        transform: inherit !important;
        white-space: initial;
        .thumb {
          padding: 0px;
          border: 0px !important;
          margin-right: 8px;
          margin-bottom: 8px;
          width: ${({ mediaLength }) =>
            mediaLength > 5
              ? 'calc(100%/6 - 7px)'
              : mediaLength === 5
              ? 'calc(100%/5 - 7px)'
              : '160px'} !important;
          height: ${({ mediaLength }) =>
            mediaLength > 5 ? '80px' : mediaLength === 5 ? '100px' : '120px'} !important;
          min-width: 70px;
          display: inline-block;
          vertical-align: top;
          ${props => props.theme.breakpoints.down('sm')} {
            width: 110px !important;
            height: 110px !important;
          }

          img {
            padding-left: 3px;
            padding-right: 3px;
          }
          &:last-child {
            margin-right: 0px;
          }
          .background {
            align-items: center;
          }
        }
      }
      .control-arrow {
        background: ${theme.colors.n90};
        height: 36px;
        display: none;
      }
    }

    &.carousel-slider {
      position: relative;
      width: 100%;
      padding-top: 56.25%;
      height: 0px;
      .slider-wrapper {
        position: absolute;
        align-items: center;
        background-color: ${theme.colors.n10};
        top: 0px;
        left: 0px;
        right: 0px;
        height: 100%;
        .slider {
          height: 100%;
        }
        .slide {
          background-color: ${theme.colors.n50};
          height: 100%;
          & > div:first-child {
            height: 100%;
          }
          img {
            width: auto;
            max-width: 100%;
          }
        }
      }
      .control-arrow {
        top: 50%;
        color: ${theme.colors.white};
        font-size: 26px;
        bottom: 0;
        margin-top: 0;
        padding: 5px;
        transform: translateY(-50%);
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        height: 64px;
        width: 50px;
        background: rgba(255, 255, 255, 0.65) !important;
        opacity: 1;
        &.control-disabled.control-arrow {
          display: ${({ showArrows }) => (showArrows ? 'block' : 'none')};
          &::after,
          &::before {
            opacity: 0.2;
          }
        }

        &::before,
        &::after {
          content: '';
          border: 0px;
          position: absolute;
          top: 35px;
          left: 0;
          right: 0;
          margin: 0px auto;
          height: 2px;
          width: 8px;
          background: ${theme.colors.n90};
          transform: skew(0deg, 41deg);
          -ms-transform: skew(0deg, 41deg);
          -webkit-transform: skew(0deg, 41deg);
        }

        &::after {
          transform: skew(0deg, -42deg);
          top: 28px;
        }

        &.control-prev {
          border-top-right-radius: 40px;
          border-bottom-right-radius: 40px;
        }
        &.control-next {
          border-top-left-radius: 40px;
          border-bottom-left-radius: 40px;
          ::before {
            top: 28px;
          }
          ::after {
            top: 35px;
          }
        }
      }
    }
    .carousel-status,
    .control-dots {
      display: none;
    }
  }
`

const isNumeric = str => {
  if (!isString(str)) return false // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
  )
}

const DetailsHoc = ({ data }) => {
  const cardSpecs = get(data, 'nftMetadata', {})
  const mediaList = [
    {
      type: 'video',
      url: get(cardSpecs, 'video', ''),
      thumbnailUrl: get(cardSpecs, 'image', ''),
      title: 'Front Animated',
    },
    {
      type: 'back',
      url: get(cardSpecs, 'back_image', ''),
      title: 'Back Static',
    },
  ]

  // type is being converted to lower case first and then comparison is started
  const getCardName = type => {
    if (isNumeric(type)) return 'Raven'
    else if (includes(type, 'king')) return 'Odin'
    else if (includes(type, 'queen')) return 'Freya'
    else if (includes(type, 'jack')) return 'Thor'
    else if (includes(type, 'ace')) return 'Ace'
    else return 'Loki'
  }

  return (
    <Container>
      {!isEmpty(cardSpecs) ? (
        <>
          <Heading>
            <H3>{getCardName(toLower(get(cardSpecs, 'attributes.card_type', '1')))}</H3>
          </Heading>
          <SliderMediaStyle>
            <CarouselComponent mediaList={mediaList} />
          </SliderMediaStyle>
        </>
      ) : null}
      <CardDetails
        details={cardSpecs}
        nftData={data}
        nftDataLoading={false}
        nftError={isEmpty(data)}
      />
    </Container>
  )
}

export default DetailsHoc
