import React from 'react'
import PropTypes from 'prop-types'
import { IconButton, Link } from '@material-ui/core'
import { Facebook, Instagram, Twitter } from '@material-ui/icons'
import styled from 'styled-components'

// Local imports
import theme from 'components/Theme'

const SocialContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: ${({ marginRight }) => (marginRight ? '5%' : '0%')};

  .icon-button {
    padding: 5px;
    margin: 6px;
    background-color: ${theme.colors.white};
    :hover {
      background-color: ${theme.colors.n10};
    }
  }
  svg {
    color: ${theme.colors.n80};
  }
  ${props => props.theme.breakpoints.down('sm')} {
    margin-right: 1%;
  }
`

export const SocialIcons = ({ marginRight = false }) => {
  return (
    <SocialContainer marginRight={marginRight}>
      <Link target="_blank" href="#" rel="noopener" onClick={e => e.preventDefault()}>
        <IconButton className="icon-button">
          <Facebook />
        </IconButton>
      </Link>
      <Link target="_blank" href="#" rel="noopener" onClick={e => e.preventDefault()}>
        <IconButton className="icon-button">
          <Twitter />
        </IconButton>
      </Link>
      <Link target="_blank" href="#" rel="noopener" onClick={e => e.preventDefault()}>
        <IconButton className="icon-button">
          <Instagram />
        </IconButton>
      </Link>
    </SocialContainer>
  )
}

SocialIcons.propTypes = {
  marginRight: PropTypes.bool,
}

export default SocialIcons
