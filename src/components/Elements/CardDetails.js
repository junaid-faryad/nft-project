import React from 'react'
import get from 'lodash/get'
import PropTypes from 'prop-types'
import isString from 'lodash/isString'
import isEmpty from 'lodash/isEmpty'
import styled from 'styled-components'
import { Link } from '@material-ui/core'
// import { useRouter } from 'next/router'
import CircularProgress from '@material-ui/core/CircularProgress'

import { H4, Body1 } from 'components/Typography'

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1 1 50%;
  overflow: hidden;
`

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  max-width: 900px;
  margin: 0px 0px 20px;
  flex: 1 1 50%;
  overflow: hidden;
`

const Heading = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  max-width: 900px;
  width: 100%;
  margin: 20px 0px 8px;
  ${props => props.theme.breakpoints.down('xs')} {
    flex-direction: column;
    padding: 0px 12px;
  }
`

const Row = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-around;
  padding: 8px 0px;
  width: 100%;
  box-sizing: border-box;
  p,
  .link-wrapper {
    width: 50%;
  }
  ${props => props.theme.breakpoints.down('xs')} {
    flex-direction: column;
    align-items: center;
    p,
    .link-wrapper {
      width: 95%;
      text-align: center;
      word-break: break-word;
    }
  }
  .link {
    margin-left: 4px;
  }
`

const CardDetails = ({ details, nftData, nftDataLoading, nftError }) => {
  // const router = useRouter()
  const isNumeric = str => {
    if (!isString(str)) return false // we only process strings!
    return (
      !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
      !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
    )
  }

  const contractId = get(nftData, 'contractAddress', ``)
  const transactionHash = get(nftData, 'transactionHash', ``)

  return (
    <Container>
      {isEmpty(details) ? (
        <Wrapper>
          <H4>No details found for this NFT</H4>
        </Wrapper>
      ) : (
        <Wrapper>
          {/* <button onClick={getHistory}>Get History</button> */}
          <Heading>
            <H4>NFT Details</H4>
          </Heading>
          <Row>
            <Body1>Token ID</Body1>
            <Body1>
              <span>{get(nftData, 'tokenId', '')}</span>
            </Body1>
          </Row>
          <Row>
            <Body1>Contract</Body1>
            <Body1 component="div" className="link-wrapper">
              {nftError ? (
                'We could not find the contract address. Please refresh the page'
              ) : nftDataLoading ? (
                <CircularProgress size={16} />
              ) : (
                <>
                  {transactionHash.length ? (
                    <>
                      <span>
                        {contractId.length > 10
                          ? `${contractId.slice(0, 4)}...${contractId.slice(-4)}`
                          : contractId}
                      </span>
                      <span className="link">
                        (
                        <Link
                          href={
                            nftData.chain !== 'BSC'
                              ? `https://testnet.bscscan.com/address/${contractId}`
                              : `https://bscscan.com/address/${contractId}`
                          }
                          target="_blank"
                          rel="noopener">
                          View
                        </Link>
                        <span>
                          {`${
                            nftData.chain !== 'BSC' ? ' on testnet BSCscan.com' : ' on BSCscan.com)'
                          }`}
                        </span>
                      </span>
                    </>
                  ) : (
                    'n/a'
                  )}
                </>
              )}
            </Body1>
          </Row>
          <Row>
            <Body1>Owner Address</Body1>
            <Body1 component="div" className="link-wrapper">
              {nftError ? (
                'We could not find the owner address. Please refresh the page'
              ) : nftDataLoading ? (
                <CircularProgress size={16} />
              ) : (
                get(nftData, 'toWalletContract', 'n/a')
              )}
            </Body1>
          </Row>
          <Row>
            <Body1>Transaction</Body1>
            <Body1 component="div" className="link-wrapper">
              {nftError ? (
                'We could not find the transaction address. Please refresh the page'
              ) : nftDataLoading ? (
                <CircularProgress size={16} />
              ) : (
                <>
                  {transactionHash.length ? (
                    <>
                      <span>
                        {transactionHash.length > 10
                          ? `${transactionHash.slice(0, 4)}...${transactionHash.slice(-4)}`
                          : transactionHash}
                      </span>
                      <span className="link">
                        (
                        <Link
                          href={
                            nftData.chain !== 'BSC'
                              ? `https://testnet.bscscan.com/tx/${transactionHash}`
                              : `https://bscscan.com/tx/${transactionHash}`
                          }
                          target="_blank"
                          rel="noopener">
                          View
                        </Link>
                        <span>{`${
                          nftData.chain !== 'BSC' ? ' on testnet BSCscan.com' : ' on BSCscan.com)'
                        }`}</span>
                      </span>
                    </>
                  ) : (
                    'n/a'
                  )}
                </>
              )}
            </Body1>
          </Row>
          <Row>
            <Body1>Blockchain</Body1>
            <Body1>{`Binance Smart Chain (${
              nftData.chain === 'BSC' ? 'BSC' : 'BSC Tesnet'
            })`}</Body1>
          </Row>
          <Row>
            <Body1>Series</Body1>
            <Body1>{get(details, 'series', 'Norse Gods Genesis 01')}</Body1>
          </Row>
          <Row>
            <Body1>Rarity</Body1>
            <Body1>{get(details, 'rarity', 'N/A')}</Body1>
          </Row>
          <Row>
            <Body1>Description</Body1>
            <Body1>{get(details, 'description', '')}</Body1>
          </Row>
          {isNumeric(get(details, 'attributes.card_type', '')) ? (
            <>
              {/* {get(details, 'attributes.Eyes', false) ? (
                <Row>
                  <Body1>Eyes</Body1>
                  <Body1>{get(details, 'attributes.Eyes', 'none')}</Body1>
                </Row>
              ) : null} */}
              {get(details, 'attributes.card_type', false) ? (
                <Row>
                  <Body1>Face</Body1>
                  <Body1>{get(details, 'attributes.card_type', 'none')}</Body1>
                </Row>
              ) : null}
              {/* {get(details, 'attributes.Hat', false) ? (
                <Row>
                  <Body1>Hat</Body1>
                  <Body1>{get(details, 'attributes.Hat', 'none')}</Body1>
                </Row>
              ) : null} */}
              {get(details, 'attributes.Suit', false) ? (
                <Row>
                  <Body1>Suit</Body1>
                  <Body1>{get(details, 'attributes.Suit', 'none')}</Body1>
                </Row>
              ) : null}
              {/* {get(details, 'attributes.Skin', false) ? (
                <Row>
                  <Body1>Skin</Body1>
                  <Body1>{get(details, 'attributes.Skin', 'none')}</Body1>
                </Row>
              ) : null} */}
              {/* {get(details, 'attributes.Weapon', false) ? (
                <Row>
                  <Body1>Weapon</Body1>
                  <Body1>{get(details, 'attributes.Weapon', 'none')}</Body1>
                </Row>
              ) : null}
              {get(details, 'attributes.Bonus', false) ? (
                <Row>
                  <Body1>Bonus</Body1>
                  <Body1>{get(details, 'attributes.Bonus', '') || 'none'}</Body1>
                </Row>
              ) : null} */}
            </>
          ) : (
            <>
              {/* <Row>
                <Body1>Eyes</Body1>
                <Body1>{get(details, 'attributes.Eyes', 'none')}</Body1>
              </Row> */}
              <Row>
                <Body1>Face</Body1>
                <Body1>{get(details, 'attributes.card_type', 'none')}</Body1>
              </Row>
              {/* <Row>
                <Body1>Hat</Body1>
                <Body1>{get(details, 'attributes.Hat', 'none')}</Body1>
              </Row> */}
              <Row>
                <Body1>Suit</Body1>
                <Body1>{get(details, 'attributes.Suit', 'none')}</Body1>
              </Row>
              {/* <Row>
                <Body1>Skin</Body1>
                <Body1>{get(details, 'attributes.Skin', 'none')}</Body1>
              </Row>
              <Row>
                <Body1>Weapon</Body1>
                <Body1>{get(details, 'attributes.Weapon', 'none')}</Body1>
              </Row>
              <Row>
                <Body1>Bonus</Body1>
                <Body1>{get(details, 'attributes.Bonus', '') || 'none'}</Body1>
              </Row> */}
            </>
          )}
        </Wrapper>
      )}
    </Container>
  )
}

CardDetails.propTypes = {
  details: PropTypes.object,
  nftData: PropTypes.object,
  nftDataLoading: PropTypes.bool,
  nftError: PropTypes.bool,
}

export default CardDetails
