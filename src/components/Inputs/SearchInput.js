import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import SearchIcon from '@material-ui/icons/Search'
import ClearIcon from '@material-ui/icons/Clear'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'

const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 600,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
    position: 'relative',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  progress: {
    position: 'absolute',
    left: '45%',
  },
}))

const SearchInput = ({
  searchText = '',
  placeholder = 'Search',
  onChange = null,
  onEnter = null,
  onSearchClick = null,
  onClearClick = () => {},
  searching = false,
}) => {
  const [value, setValue] = useState(searchText)
  const classes = useStyles()

  useEffect(() => {
    setValue(searchText)
  }, [searchText])

  const handleSubmit = event => {
    if (onChange) {
      onChange(value)
    }
  }
  const handleEnterKey = e => {
    const isEnterPressed = e.key === 'Enter'
    if (isEnterPressed && onChange) {
      onChange(value)
    }
  }
  const handleClearIcon = () => {
    if (onClearClick) onClearClick('')
    setValue('')
  }

  return (
    <Paper className={classes.root}>
      {searching ? <CircularProgress size={28} className={classes.progress} /> : null}
      <InputBase
        className={classes.input}
        placeholder={placeholder}
        onChange={e => setValue(e.target.value)}
        inputProps={{ 'aria-label': placeholder }}
        value={value}
        onKeyPress={handleEnterKey}
        disabled={searching}
      />
      <IconButton
        disabled={searching}
        onClick={handleClearIcon}
        className={classes.iconButton}
        aria-label="clear">
        <ClearIcon />
      </IconButton>
      <Divider className={classes.divider} orientation="vertical" />
      <IconButton
        disabled={searching}
        onClick={handleSubmit}
        color="primary"
        className={classes.iconButton}
        aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  )
}

SearchInput.propTypes = {
  searchText: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onEnter: PropTypes.func,
  onSearchClick: PropTypes.func,
  onClearClick: PropTypes.func,
  searching: PropTypes.bool,
}

export default SearchInput
