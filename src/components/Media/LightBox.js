import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Dialog, DialogContent, Slide, IconButton } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import CloseIcon from '@material-ui/icons/Close'
import ReactPlayer from 'react-player/lazy'
import get from 'lodash/get'
import includes from 'lodash/includes'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
// import Description from '@material-ui/icons/Description'
// import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf'
// import CloudDownloadIcon from '@material-ui/icons/CloudDownloadOutlined'

import { FontWeights, H6 } from 'components/Typography'
import Loading from 'components/Loading'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const useStyles = makeStyles(theme => ({
  dialog: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  root: {
    maxWidth: 1070,
    background: 'rgb(27, 38, 56)',
    minHeight: 300,
    height: '90vh',
    [theme.breakpoints.down('sm')]: {
      minHeight: 'auto',
    },
  },
  paperFullWidth: {
    width: 'calc(100% - 64px)',
    [theme.breakpoints.down('md')]: {
      width: 'calc(100% - 32px)',
    },
    [theme.breakpoints.down('xs')]: {
      maxWidth: '100% !important',
      width: '100%',
      padding: '0px 8px',
      margin: 0,
    },
  },
  paper: {
    margin: 0,
    background: 'rgb(27, 38, 56)',
    [theme.breakpoints.down('md')]: {
      margin: '0px 8px',
    },
    [theme.breakpoints.down('xs')]: {
      margin: 0,
    },
  },
  body: {
    display: 'flex',
    width: '100%',
    height: '100%',
    margin: 'auto 0px',
    position: 'relative',
    '& .MuiDialogContent-root': {
      padding: 0,
      position: 'relative',
      margin: 'auto',
      height: '100%',
    },
    '& .player-container': {
      width: '100%',
      position: 'relative',
      paddingTop: '56.25%', // used to make the player responsive
    },
    '& .react-player': {
      position: 'absolute',
      top: '0',
      left: '0',
    },
  },
  videoWrapper: {
    width: `90%`,
    maxWidth: 960,
    margin: `auto`,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  closeIconButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    background: 'rgba(255, 255, 255, 0.5)',
    padding: 4,
    zIndex: 1,
    '&:hover': {
      background: 'rgba(255, 255, 255, 0.7)',
    },
    '& svg': {
      color: theme.core.colors.black,
    },
  },
  backIcon: {
    position: 'absolute',
    top: '45%',
    left: '1%',
    padding: 1,
    zIndex: 1,
    cursor: 'pointer',
    background: theme.core.colors.white,
    '&:hover': {
      background: theme.core.colors.n10,
    },
    '& svg': {
      color: theme.core.colors.primaryColor,
    },
  },
  nextIcon: {
    position: 'absolute',
    top: '45%',
    right: '1%',
    background: theme.core.colors.white,
    padding: 1,
    zIndex: 1,
    cursor: 'pointer',
    '&:hover': {
      background: theme.core.colors.n10,
    },
    '& svg': {
      color: theme.core.colors.primaryColor,
    },
  },
  imageContainer: {
    width: '100%',
    padding: '12px 0px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 'auto',
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      padding: '12px 20px',
    },
    '& img': {
      borderRadius: 8,
      width: '92%',
      height: '80vh',
      objectFit: 'contain',
      [theme.breakpoints.down('sm')]: {
        width: '95%',
      },
    },
  },
  containerWithFileName: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '100%',
    minHeight: 400,
    height: '100%',
    cursor: 'pointer',
    backgroundColor: 'transparent',
    '& svg': {
      color: theme.core.colors.white,
      paddingBottom: '12px',
      height: '50px',
      width: '50px',
    },
  },
  rowTextIcon: {
    flexDirection: 'row',
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    justifyContent: 'center',
    color: theme.core.colors.white,
  },
  count: {
    position: 'absolute',
    top: '2%',
    left: '50%',
    color: theme.core.colors.white,
  },
  loading: {
    height: '90%',
    width: '90%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    margin: '0px 5%',
    '& .MuiCircularProgress-colorPrimary': {
      color: theme.core.colors.white,
    },
  },
}))

const ImageViewer = ({
  onClose,
  isOpen,
  playerControls = true,
  lightMode = false,
  handlePipToggle,
  autoPlay = true,
  current,
  mediaList,
}) => {
  const classes = useStyles()
  const [currentMedia, setCurrentMedia] = useState(mediaList[current])
  const [currentIndex, setCurrentIndex] = useState(current)
  const handleNavigation = index => {
    setCurrentIndex(index)
    setCurrentMedia(mediaList[index])
    if (includes(get(mediaList[index], 'node.mimeType', ''), 'image')) setLoading(true)
  }
  const [loading, setLoading] = useState(true)
  const imageLoaded = () => {
    setLoading(false)
  }
  return (
    <Dialog
      aria-labelledby="light-box"
      disableBackdropClick
      fullWidth={true}
      maxWidth={'md'}
      open={isOpen}
      scroll={'body'}
      TransitionComponent={Transition}
      keepMounted
      classes={{
        paperWidthMd: classes.root,
        paperFullWidth: classes.paperFullWidth,
        paper: classes.paper,
        root: classes.dialog,
      }}
      onClose={onClose}>
      <div className={classes.count}>
        <H6 fontWeight={FontWeights.regular}>{currentIndex + 1 + '/' + mediaList.length}</H6>
      </div>
      <IconButton className={classes.closeIconButton} onClick={onClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
      <div className={classes.body}>
        <DialogContent>
          {mediaList.length > 1 ? (
            <>
              <IconButton
                className={classes.backIcon}
                onClick={e => {
                  e.stopPropagation()
                  currentIndex === 0
                    ? handleNavigation(mediaList.length - 1)
                    : handleNavigation(currentIndex - 1)
                }}>
                <ChevronLeftIcon />
              </IconButton>
              <IconButton
                className={classes.nextIcon}
                onClick={e => {
                  e.stopPropagation()
                  currentIndex === mediaList.length - 1
                    ? handleNavigation(0)
                    : handleNavigation(currentIndex + 1)
                }}>
                <ChevronRightIcon />
              </IconButton>
            </>
          ) : null}
          {!includes(get(currentMedia, 'type', ''), 'video') ? (
            <>
              {loading ? (
                <div className={classes.loading}>
                  <Loading />
                </div>
              ) : null}
              <div
                className={classes.imageContainer}
                style={{ visibility: loading ? 'hidden' : 'visible' }}>
                <img
                  src={get(currentMedia, 'url', '')}
                  alt=""
                  loading="lazy"
                  onLoad={imageLoaded}
                />
              </div>
            </>
          ) : (
            <div className={classes.videoWrapper}>
              <div className="player-container">
                <ReactPlayer
                  url={get(currentMedia, 'url', '')}
                  className="react-player"
                  width="100%"
                  height="100%"
                  controls={playerControls}
                  playing={autoPlay}
                  light={lightMode}
                  stopOnUnmount={false}
                  onEnablePIP={handlePipToggle}
                  onDisablePIP={handlePipToggle}
                  pip={false} // picture in picture mode
                  // config={{ file: { attributes: { disablepictureinpicture: 'true' } } }} to disable the picture in picture mode
                />
              </div>
            </div>
          )}
          {/* : (
            <Box
              title={'download'}
              className={classes.containerWithFileName}
              onClick={() => window.open(get(currentMedia, 'node.url', ''), '_blank')}>
              <CloudDownloadIcon className={classes.largeIcon} />
              {get(currentMedia, 'node.fileName', false) ? (
                <div className={classes.rowTextIcon}>
                  {includes(get(currentMedia, 'node.mimeType', ''), '/pdf') ? (
                    <PictureAsPdfIcon color="primary" style={{ fontSize: 22, marginRight: 6 }} />
                  ) : (
                    <Description color="primary" style={{ fontSize: 22, marginRight: 6 }} />
                  )}
                  <H6 fontWeight={FontWeights.medium}>{get(currentMedia, 'node.fileName', '')}</H6>
                </div>
              ) : null}
            </Box>
          )} */}
        </DialogContent>
      </div>
    </Dialog>
  )
}

ImageViewer.propTypes = {
  onClose: PropTypes.func,
  isOpen: PropTypes.bool,
  autoPlay: PropTypes.bool,
  playerControls: PropTypes.bool,
  lightMode: PropTypes.bool,
  handlePipToggle: PropTypes.func,
  current: PropTypes.number,
  mediaList: PropTypes.array,
}

export default ImageViewer
