import React from 'react'
import get from 'lodash/get'
import isString from 'lodash/isString'
import toLower from 'lodash/toLower'
import includes from 'lodash/includes'
import Head from 'next/head'
import gql from 'graphql-tag'
import toUpper from 'lodash/toUpper'

import ApiRequest from 'util/network/apiGraphql'
import DetailsHoc from 'components/HOC/NftDetails'

const query = gql`
  query($wallet: String!) {
    findNftsByWallet(wallet: $wallet) {
      id
      contractAddress
      nftId
      nftMetadata
      toWalletContract
      transactionHash
      tokenId
      chain
    }
  }
`

const isNumeric = str => {
  if (!isString(str)) return false // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
  )
}

const HomePage = ({ data }) => {
  const cardSpecs = get(data, 'nftMetadata', {})
  // type is being converted to lower case first and then comparison is started
  const getCardName = type => {
    if (isNumeric(type)) return 'Raven'
    else if (includes(type, 'king')) return 'Odin'
    else if (includes(type, 'queen')) return 'Freya'
    else if (includes(type, 'jack')) return 'Thor'
    else if (includes(type, 'ace')) return 'Ace'
    else return 'Loki'
  }

  const getPageDescription = () => {
    const description = get(
      cardSpecs,
      'description',
      'NFT based Poker game to win exciting prices'
    ).slice(0, 100)
    return `FullHouse | ${description}`
  }

  return (
    <>
      <Head>
        <title>
          {getCardName(toLower(get(cardSpecs, 'attributes.card_type', '1')))} Card | Fullhouse - NFT
          Poker Game
        </title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content={getPageDescription()} />
        <meta
          property="og:title"
          content={`${getCardName(
            toLower(get(cardSpecs, 'attributes.card_type', '1'))
          )} Card | Fullhouse - NFT Poker Game`}
        />
        <meta property="og:description" content={getPageDescription()} />
        <meta property="og:image" content={`${get(cardSpecs, 'image', '')}`} />
        <meta property="og:type" content="website" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="628" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Fullhouse" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta
          name="twitter:title"
          content={`${getCardName(
            toLower(get(cardSpecs, 'attributes.card_type', '1'))
          )} Card | Fullhouse - NFT Poker Game`}
        />
        <meta name="twitter:description" content={getPageDescription()} />
        <meta name="”twitter:image”" content={`${get(cardSpecs, 'image', '')}`} />
      </Head>
      <DetailsHoc data={data} />
    </>
  )
}

export const getServerSideProps = async ctx => {
  const wallet = get(ctx, 'query.address', '')
  let data = {}
  try {
    const convertedWallet = toUpper(wallet)
    const response = await ApiRequest(query, { wallet: convertedWallet })
    data = get(response, 'findNftsByWallet', {})
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err)
  }
  return {
    props: {
      data,
    },
  }
}

export default HomePage
