import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'
import toUpper from 'lodash/toUpper'
import gql from 'graphql-tag'
import toLower from 'lodash/toLower'
import isString from 'lodash/isString'
import includes from 'lodash/includes'
import Link from '@material-ui/core/Link'

import { H2, Body1, FontWeights } from 'components/Typography'
// import { H1, H6 } from 'components/Typography'
// import SocialIcons from 'components/Elements/SocialIcons'
import theme from 'components/Theme'
import SearchInput from 'components/Inputs/SearchInput'
import ApiRequest from 'util/network/apiGraphql'
import DetailsHoc from 'components/HOC/NftDetails'
import ProgressLoading from 'components/Loading'

const query = gql`
  query($wallet: String!) {
    findNftsByWallet(wallet: $wallet) {
      id
      contractAddress
      nftId
      nftMetadata
      toWalletContract
      transactionHash
      tokenId
      chain
    }
  }
`

const Container = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  padding-top: ${({ searchResult }) => (searchResult ? '1%' : '5%')};
  min-height: 500px;
  height: 100%;
  flex-direction: column;

  ${props => props.theme.breakpoints.down('sm')} {
    padding: 20px 16px 0px;
  }

  ${props => props.theme.breakpoints.down('xs')} {
    justify-content: flex-start;
  }

  /* .main {
    font-size: 7rem;
    line-height: 8rem;
    margin-bottom: 20px;
    color: ${theme.colors.primaryColor};
    text-align: center;
    text-shadow: 10px 10px 25px rgb(81 67 21), -10px 10px 25px rgb(81 67 21),
      -10px -10px 25px rgb(81 67 21), 10px -10px 25px rgb(81 67 21);
    ${props => props.theme.breakpoints.down('sm')} {
      font-size: 4rem;
      line-height: 5rem;
    }
  } */
  .search-area {
    margin: ${({ searchResult }) => (searchResult ? '20px 0px' : '20px 0px 50px')};
    ${props => props.theme.breakpoints.down('xs')} {
      width: 100%;
    }
  }

  h2 {
    margin-bottom: 20px;
  }

  .not-found {
    p {
      margin-top: 12px;
    }
  }

  .announcement {
    font-size: 20px;
    line-height: 26px;
    color: ${theme.colors.n20};
    margin-bottom: 12px;
    max-width: 800px;
    padding: 0px 16px;
  }

  .footer {
    color: ${theme.colors.n20};
    margin-top: auto;
    padding: 12px 16px;
  }

  .theme-color {
    color: ${theme.colors.primaryColor};
  }
`

export const getServerSideProps = async ctx => {
  const wallet = get(ctx, 'query.address', '')
  if (wallet) {
    let data = {}
    try {
      const convertedWallet = toUpper(wallet)
      const response = await ApiRequest(query, { wallet: convertedWallet })
      data = get(response, 'findNftsByWallet', {})
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err)
    }
    return {
      props: {
        data,
      },
    }
  } else return { props: { data: {} } }
}

const isNumeric = str => {
  if (!isString(str)) return false // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
  )
}

const HomePage = ({ data = {} }) => {
  const router = useRouter()
  const queryAddress = get(router, 'query.address', '')

  // wallet address
  const [address, setAddress] = useState(get(router, 'query.address', ''))
  const [searching, setSearching] = useState(false)
  const [notFound, setNotFound] = useState(queryAddress ? isEmpty(data) : false)
  const [nftData, setNftData] = useState(data)

  const fetchNftData = async wallet => {
    router.push(`/?address=${wallet}`)
    try {
      const convertedWallet = toUpper(wallet)
      const response = await ApiRequest(query, { wallet: convertedWallet })
      const responseData = get(response, 'findNftsByWallet', {})
      if (!isEmpty(responseData)) {
        setNftData(responseData)
        setSearching(false)
        setNotFound(false)
      } else {
        setSearching(false)
        setNotFound(true)
        setNftData('')
      }
    } catch (err) {
      setSearching(false)
      setNotFound(true)
      setNftData('')
      // eslint-disable-next-line no-console
      console.log(err)
    }
  }

  const handleSearch = value => {
    setAddress(value)
    setSearching(true)
    fetchNftData(value)
  }

  const getCardName = type => {
    if (isNumeric(type)) return 'Raven'
    else if (includes(type, 'king')) return 'Odin'
    else if (includes(type, 'queen')) return 'Freya'
    else if (includes(type, 'jack')) return 'Thor'
    else if (includes(type, 'ace')) return 'Ace'
    else return 'Loki'
  }
  const getCardTitle = data => {
    if (isEmpty(data)) return 'Fullhouse - NFT Poker Game'
    else {
      return `${getCardName(
        toLower(get(data, 'nftMetadata.attributes.card_type', '1'))
      )} Card | Fullhouse - NFT
          Poker Game`
    }
  }
  const getPageDescription = data => {
    if (isEmpty(data)) return 'NFT based Poker game to win exciting prices | Fullhouse'
    else {
      const description = get(
        data,
        'nftMetadata.description',
        'NFT based Poker game to win exciting prices'
      ).slice(0, 100)
      return `FullHouse | ${description}`
    }
  }
  useEffect(() => {
    if (isEmpty(queryAddress)) {
      if (!isEmpty(address)) setAddress('')
      if (!isEmpty(nftData)) setNftData({})
      if (notFound) setNotFound(false)
    }
    if (queryAddress !== address && !isEmpty(queryAddress)) handleSearch(queryAddress)
  }, [queryAddress])

  return (
    <>
      <Head>
        <title>Fullhouse - NFT Poker Game</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

        <title>{getCardTitle(nftData)}</title>
        <meta name="description" content={getPageDescription(nftData)} />
        <meta property="og:title" content={getCardTitle(nftData)} />
        <meta property="og:description" content={getPageDescription(nftData)} />
        <meta property="og:image" content={get(nftData, 'nftMetadata.image', '')} />
        <meta property="og:type" content="website" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="628" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Fullhouse" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content={getCardTitle(nftData)} />
        <meta name="twitter:description" content={getPageDescription(nftData)} />
        <meta name="”twitter:image”" content={get(nftData, 'nftMetadata.image', '')} />
      </Head>
      <Container searchResult={!isEmpty(nftData)}>
        {!notFound && isEmpty(nftData) && !searching ? (
          <>
            <Body1 fontWeight={FontWeights.regular} className="announcement">
              As a thank you to our community, the top 1,000 CBC wallets on the Ethereum blockchain
              on May 26, 12:00 UTC were each dropped a Norse Gods Genesis playing card NFT on
              Binance Smart Chain!
            </Body1>
            <Body1 fontWeight={FontWeights.regular} className="announcement">
              You can check if your NFT qualified by entering the Ethereum wallet in which you hold
              your CBC in the box below:
            </Body1>
          </>
        ) : null}
        <H2>Search NFT</H2>
        <div className="search-area">
          <SearchInput
            onChange={value => handleSearch(value)}
            placeholder={'Search by Wallet Address'}
            searchText={address}
            onClearClick={() => setAddress('')}
            searching={searching}
          />
        </div>
        {!notFound && isEmpty(nftData) && !searching ? (
          <>
            <Body1 fontWeight={FontWeights.regular} className="announcement">
              Only <strong>10,000</strong> Norse Gods Genesis NFTs will ever be issued. They can be
              used to compete for big CBC prizes in our unique NFT poker game, launching in July at{' '}
              <Link href="https://www.fullhouse.io/" target="_blank" rel="noreferrer">
                FullHouse.io
              </Link>
            </Body1>
            <Body1 fontWeight={FontWeights.regular} className="announcement">
              For more information and to find out how to take part in future drops, please visit{' '}
              <Link href="http://www.cbc.network/" target="_blank" rel="noreferrer">
                www.cbc.network
              </Link>{' '}
              and{' '}
              <Link href="https://t.me/casinobettingcoin" target="_blank" rel="noreferrer">
                join us on Telegram
              </Link>
              .
            </Body1>
            <Body1 fontWeight={FontWeights.regular} className="footer">
              Please note, the NFT has been dropped to the Binance Smart Chain wallet with the same
              address. For more information on accessing that wallet via MetaMask,{' '}
              <Link
                href="https://academy.binance.com/en/articles/connecting-metamask-to-binance-smart-chain"
                target="_blank"
                rel="noreferrer">
                please click here.
              </Link>
            </Body1>
          </>
        ) : null}
        {(notFound && !searching) || (queryAddress && isEmpty(nftData) && !searching) ? (
          <div className="not-found">
            <Body1>
              No NFTs found for this address. Please make sure to enter the correct wallet address.
            </Body1>
          </div>
        ) : null}
        {searching ? <ProgressLoading /> : isEmpty(nftData) ? null : <DetailsHoc data={nftData} />}
      </Container>
    </>
  )
}

export default HomePage
