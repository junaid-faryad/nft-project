import React from 'react'
import Head from 'next/head'
import styled from 'styled-components'

import { H4, H1 } from 'components/Typography'
import theme from 'components/Theme'

const Container = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin: auto;
  min-height: 500px;
  padding: 0px 20px;

  h1 {
    font-size: 7rem;
    line-height: 8rem;
    margin-bottom: 20px;
    color: ${theme.colors.primaryColor};
    text-align: center;
    text-shadow: 10px 10px 25px rgb(81 67 21), -10px 10px 25px rgb(81 67 21),
      -10px -10px 25px rgb(81 67 21), 10px -10px 25px rgb(81 67 21);
    ${props => props.theme.breakpoints.down('sm')} {
      font-size: 4rem;
      line-height: 5rem;
    }
  }
`

const HomePage = ({ cardSpecs }) => {
  return (
    <>
      <Head>
        <title>Fullhouse - NFT Poker</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Container>
        <H1>404</H1>
        <H4>The Page you request could not be found</H4>
      </Container>
    </>
  )
}

export default HomePage
