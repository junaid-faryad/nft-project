// https://nextjs.org/docs/api-reference/next.config.js/introduction
/*
  Avoid using new JavaScript features not available in your target Node.js version.
  next.config.js will not be parsed by Webpack, Babel or TypeScript.
 */

module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    if (dev) {
      config.module.rules.push({
        test: /\.(j|t)sx?$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
      })
    }
    return config
  },
}
